declare module '*.svg?inline' {
  const content: any
  export default content
}
declare module 'v-click-outside' {
  const content: any;
  export default content
}
declare module '@/data/pages.json' {
  interface Items {
    [index: string]: {
      name: string;
      items: Items;
    };
  }
  const items: Items
  export default items
}
