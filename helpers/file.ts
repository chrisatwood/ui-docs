export function getExtension (path: string): string {
  const seps = path.split('.')
  const extension = seps[seps.length - 1]

  return extension.toLowerCase()
}
export function hasLogo (path: string) {
  const supportFiles = ['ts', 'sass', 'html']

  return supportFiles.includes(getExtension(path))
}
