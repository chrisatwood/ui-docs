export const randomString = (length = 6): string => {
  return Math.random()
    .toString(20)
    .substr(2, length)
}

export const camelCase = (string: string): string =>
  string
    .replace(/(?:^\w|[A-Z]|\b\w)/g, (word: string, index: number): string => {
      return index === 0 ? word.toLowerCase() : word.toUpperCase()
    })
    .replace(/\s+/g, '')

export const pascalCase = (string: string): string => {
  const cam = camelCase(string)
  return cam.charAt(0).toUpperCase() + cam.slice(1)
}
