export function getUnits (
  px: string
): { raw: number; px: string; rem: { raw: number; format: string } } {
  const raw = removeUnit(px)

  return {
    raw,
    px,
    rem: {
      raw: raw / 16,
      format: `${raw / 16}rem`
    }
  }
}

function removeUnit (str: string) {
  return parseFloat(str.replace('px', ''))
}
