import { contentFunc } from '@nuxt/content/types/content'
import { SiteMapItem, ContentItem } from '@/index'

export function getPages ($content: contentFunc, path: string) {
  return new Promise<SiteMapItem>((resolve, reject) => {
    const pageTitle = `${path.charAt(1).toUpperCase()}${path
      .substring(2)
      .replace(/-/, ' ')}`

    $content(path, {
      deep: true
    })
      .only(['title', 'path', 'dir'])
      .sortBy('dir')
      .sortBy('title')
      .fetch()
      .then((pages) => {
        const item: SiteMapItem = {
          title: pageTitle,
          path,
          items: formatContentArray((pages as unknown) as ContentItem[])
        }

        resolve(item)
      })
      .catch((err: any) => {
        console.error(err)

        reject(new Error('Page not found'))
      })
  })
}

export function formatContentArray (data: ContentItem[]): SiteMapItem[] {
  return data.map((item) => {
    return {
      items: [],
      title: item.title,
      path: item.path
    }
  })
}
