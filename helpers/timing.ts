export const throttle = function (callback: any) {
  window.requestAnimationFrame((): void => {
    callback()
  })
}
