import Vue from 'vue'
export const CodeMixin = Vue.extend({
  data () {
    return {
      code: ''
    }
  },
  methods: {
    updateCode (html: string) {
      this.$nextTick().then(() => {
        this.code = html.replace(/<!---->/g, '')
      })
    }
  }
})
