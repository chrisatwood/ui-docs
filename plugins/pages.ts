import { SiteMapItem } from '@/index'
import { Plugin } from '@nuxt/types'
import { getPages } from '@/helpers/pages'

declare module 'vue/types/vue' {
  // this.$myInjectedFunction inside Vue components
  interface Vue {
    $pages: { [index: string]: SiteMapItem }
  }
}

declare module '@nuxt/types' {
  // nuxtContext.app.$myInjectedFunction inside asyncData, fetch, plugins, middleware, nuxtServerInit
  interface NuxtAppOptions {
    $pages: { [index: string]: SiteMapItem }
  }
  // nuxtContext.$myInjectedFunction
  interface Context {
    $pages: { [index: string]: SiteMapItem }
  }
}

const pagesPlugin: Plugin = async ({ $content }, inject) => {
  const structure: { [index: string]: SiteMapItem } = {}

  // Inject $hello(msg) in Vue, context and store.

  /* Getting started */
  structure['/getting-started'] = await getPages(
    $content,
    '/getting-started'
  ).catch((err: any) => {
    console.error(err)
    return { items: [], title: '', path: '/' }
    // this.$nuxt.error({ statusCode: 404, message: 'Page not found' })
  })

  /* components */
  structure['/components'] = await getPages($content, '/components').catch(
    (err: any) => {
      console.error(err)
      return { items: [], title: '', path: '/' }
      // this.$nuxt.error({ statusCode: 404, message: 'Page not found' })
    }
  )

  /* content */
  structure['/content'] = await getPages($content, '/content').catch(
    (err: any) => {
      console.error(err)
      return { items: [], title: '', path: '/' }
      // this.$nuxt.error({ statusCode: 404, message: 'Page not found' })
    }
  )

  /* content */
  structure['/utilities'] = await getPages($content, '/utilities').catch(
    (err: any) => {
      console.error(err)
      return { items: [], title: '', path: '/' }
      // this.$nuxt.error({ statusCode: 404, message: 'Page not found' })
    }
  )

  /* API */
  structure['/api'] = await getPages($content, '/api').catch((err: any) => {
    console.error(err)
    return { items: [], title: '', path: '/' }
    // this.$nuxt.error({ statusCode: 404, message: 'Page not found' })
  })

  console.log(structure)

  inject('pages', structure)
}

export default pagesPlugin
