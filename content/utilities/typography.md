---
title: Typography
---

## Heading and paragraph style properties

<alert colour="info">Click the heading to see it's properties</alert>

<heading-example></heading-example>

## Inline text elements

The only accepted inline text elements are listed below. There other
options, however, these are un-accessible

<inline-text-example></inline-text-example>

## Utilities

<text-utilities></text-utilities>
