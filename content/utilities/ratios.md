---
title: Ratio
input: ''
---

Ratios are created created by adding a padding-top with a specific  percentage
to force a ratio.

<ratio-calculator></ratio-calculator>

## Available ratios

- **16 by 9**: `.ratio-16-9`
