---
title: Grid layout
external: https://getbootstrap.com/docs/4.3/layout/grid/
---

The grid layout is the same used as part of the bootstrap core package. We have
altered the media queries, [you can see them here](/utilities/breakpoints).

<alert colour="info">Please see <a href="https://getbootstrap.com/docs/4.3/layout/grid/">the bootstrap documentation</a>
on the the grid layouts.</alert>
