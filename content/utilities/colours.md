---
title: Colours
description: List of all available colours and how to add new colours.

general: 
  - { name: '$colourAlto', value: '#d9d9d9' }
  - { name: '$colourConcrete', value: '#f2f2f2' }
  - { name: '$colourAzureRadiance', value: '#007bff', textColour: '#fff' }
  - { name: '$colourCararra', value: '#ebe9e1' }
  - { name: '$colourHusk', value: '#bd9e5e' }
  - { name: '$colourThunder', value: '#231f20', textColour: '#fff' }
  - { name: '$colourPickledBlueWood', value: '#2f444e', textColour: '#fff' }
  - { name: '$colourYuma', value: '#c8bb8d' }
  - { name: '$colourBlueZodiac', value: '#152c51', textColour: '#fff' }
  - { name: '$colourHaiti', value: '#191333', textColour: '#fff' }
  - { name: '$colourBossanova', value: '#3c2c59', textColour: '#fff' }
  - { name: '$colourAtlantis', value: '#8fc73e', textColour: '#fff' }
  - { name: '$colourGreenHaze', value: '#009966', textColour: '#fff' }
  - { name: '$colourPrussianBlue', value: '#002554', textColour: '#fff' }
  - { name: '$colourWildSand', value: '#f6f6f6' }
  - { name: '$colourPeppermint', value: '#dff0d8' }
  - { name: '$colourPearlLusta', value: '#fcf8e3' }
  - { name: '$colourLinkWater', value: '#d9edf7' }
  - { name: '$colourPoloBlue', value: '#7598cb', textColour: '#fff' }
  - { name: '$colourBrickRed', value: '#BA283A', textColour: '#fff' }
  - { name: '$colourPanache', value: '#F8FCFA' }
  - { name: '$colourAlabaster', value: '#f8f8f8' }
  - { name: '$colourHavelockBlue', value: '#4786d5' }
  - { name: '$colourTapa', value: '#727270', textColour: '#fff' }
  - { name: '$colourDustyGrey', value: '#999999', textColour: '#fff' }
  - { name: '$colourLocalNaviation', value: '#8a8a8a', textColour: '#fff' }
  - { name: '$colourRegentGrey', value: '#8A94A8', textColour: '#fff' }
  - { name: '$colourMineShaft', value: '#333333', textColour: '#fff' }
  - { name: '$colourCabaret', value: '#d94c5d', textColour: '#fff' }
  - { name: '$colourTimberwolf', value: '#D4D2CA' }

grayscales:
  - { value: '#373736', name: '$colourTintDarkest', textColour: '#fff' }
  - { value: '#666666', name: '$colourGreyDarkest', textColour: '#fff' }
  - { value: '#383735', name: '$colourTintDarkest', textColour: '#fff' }
  - { value: '#4e4e4c', name: '$colourGreyDark', textColour: '#fff' }
  - { value: '#555555', name: '$colourGrey', textColour: '#fff' }
  - { value: '#666564', name: '$colourTintDarker', textColour: '#fff' }
  - { value: '#7a7978', name: '$colourTintDark', textColour: '#fff' }
  - { value: '#bcbbbb', name: '$colourTintLight' }
  - { value: '#bbbbbb', name: '$colourSilver' }
  - { value: '#555555', name: '$colourGreyNickel', textColour: '#fff' }
  - { value: '#555555', name: '$colourGreyNickel', textColour: '#fff' }
  - { value: '#d0d0cf', name: '$colourTintLighter' }
  - { value: '#d3d3d2', name: '$colourPrimeGrey' }
  - { value: '#d3d3d2', name: '$colourGreyLight' }
  - { value: '#dadada', name: '$colourGreySoft' }
  - { value: '#e9e9e9', name: '$colourGreyLighter' }
  - { value: '#e9e9e9', name: '$colourTintLightest' }
  - { value: '#f0f0f0', name: '$colourGreyLightest' }
  - { value: '#f6f6f6', name: '$colourGreySofter' }
  - { value: '#f5f5f5', name: '$colourWildSandGrey' }
  - { value: '#ffffff', name: '$colourWhite' }
  
supplementary:
  - { name: '$colourOceanGreen', value: '#499f7f', textColour: '#fff' }
  - { name: '$colourAffair', value: '#73479c', textColour: '#fff' }
  - { name: '$colourAllports', value: '#006aaf', textColour: '#fff' }
  - { name: '$colourCerise', value: '#d63694', textColour: '#fff' }
  - { name: '$colourAlgaeGreen', value: '#98d0bb' }
  - { name: '$colourAeroBlue', value: '#cce8de' }
  - { name: '$colourMalibu', value: '#49b7ff' }
  - { name: '$colourAnakiwa', value: '#e2f4ff' }
  - { name: '$colourLavender', value: '#b294ce', textColour: '#fff' }
  - { name: '$colourMauve', value: '#f1ecf6' }
  - { name: '$colourHotPink', value: '#e78bc1' }
  - { name: '$colourLavenderRose', value: '#fdf5f9' }
  
textUtils:
  - { name: '.text-white', value: '#ffffff' }
  - { name: '.text-primary', value: '#9d2231', textColour: '#ffffff' }
  - { name: '.text-secondary', value: '#6c757d', textColour: '#ffffff' }
  - { name: '.text-success', value: '#008458', textColour: '#ffffff' }
  - { name: '.text-info', value: '#17a2b8', textColour: '#ffffff' }
  - { name: '.text-warning', value: '#ffc107', textColour: '#ffffff' }
  - { name: '.text-danger', value: '#d4374a', textColour: '#ffffff' }
  - { name: '.text-light', value: '#f8f9fa' }
  - { name: '.text-dark', value: '#343a40', textColour: '#ffffff' }
  - { name: '.text-body', value: '#383735', textColour: '#ffffff' }
  - { name: '.text-muted', value: '#6c757d', textColour: '#ffffff' }
  - { name: '.text-black-50 ', value: 'rgba(0, 0, 0, 0.5)', textColour: '#ffffff', }
  - { name: '.text-white-50', value: '#rgba(255, 255, 255, 0.5)' }
  - { name: '.colour-grey', value: '#555555', textColour: '#ffffff' }
  - { name: '.colour-red', value: '#d4374a', textColour: '#ffffff' }
  
bg:
  - { name: '.bg-primary', value: '#d4374a', textColour: '#ffffff' }
  - { name: '.bg-secondary', value: '#6c757d', textColour: '#ffffff' }
  - { name: '.bg-success', value: '#008458', textColour: '#ffffff' }
  - { name: '.bg-info', value: '#17a2b8', textColour: '#ffffff' }
  - { name: '.bg-warning', value: '#ffc107', textColour: '#ffffff' }
  - { name: '.bg-danger', value: '#d4374a', textColour: '#ffffff' }
  - { name: '.bg-dark', value: '#343a40', textColour: '#ffffff' }
  - { name: '.bg-white', value: '#fff' }
  - { name: '.bg-transparent', value: 'transparent' }
  - { name: '.bg-ocean-green', value: '#499f7f', textColour: '#ffffff' }
  - { name: '.bg-cerise', value: '#d63694', textColour: '#ffffff' }
  - { name: '.bg-affair', value: '#73479c', textColour: '#ffffff' }
  - { name: '.bg-panache', value: '#F8FCFA' }
  - { name: '.bg-anakiwa', value: '#e2f4ff' }
  - { name: '.bg-areo-blue', value: '#cce8de' }
  - { name: '.bg-mauve', value: '#f1ecf6' }
  - { name: '.bg-wild-sand', value: '#f6f6f6' }
  - { name: '.bg-prussian-blue', value: '#002554', textColour: '#ffffff' }

border:
  - { name: '.border-primary', value: '#d4374a', textColour: '#ffffff' }
  - { name: '.border-secondary', value: '#6c757d', textColour: '#ffffff' }
  - { name: '.border-success', value: '#008458', textColour: '#ffffff' }
  - { name: '.border-info', value: '#17a2b8', textColour: '#ffffff' }
  - { name: '.border-warning', value: '#ffc107', textColour: '#ffffff' }
  - { name: '.border-danger', value: '#d4374a', textColour: '#ffffff' }
  - { name: '.border-light', value: '#f8f9fa' }
  - { name: '.border-dark', value: '#343a40', textColour: '#ffffff' }
  - { name: '.border-white', value: '#fff' }

---

All colours can be found in `src/sass/globals/vars/colours.sass`.

## Adding new colours

All colours must be defined in `/src/sass/globals/vars/colours.sass`. When adding
new colours, please check whether the colour is already available or closely matches
 another colour.

Colours should be HEX or RGBA if transparency is required. HEX colours should be
6 characters (not the shorthand 3 characters)

The colour variable name and the class name should be generated below or using
the [tool here](http://chir.ag/projects/name-that-color/).

<colour-example :greyscale="grayscales" :general="general" :supplementary="supplementary"></colour-example>d4374a

## Available colours within SASS

### Grey scale

<colour-swatches :items="grayscales"></colour-swatches>

### Supplementary colours

<colour-swatches :items="supplementary"></colour-swatches>

### General colours

<colour-swatches :items="general"></colour-swatches>

## Available colours using classes

### Available background options

<colour-swatches  :items="bg" :min-width="150"></colour-swatches>

### Available text options

<colour-swatches  :items="textUtils" :min-width="150"></colour-swatches>

### Available border options

<colour-swatches  :items="border" :min-width="150"></colour-swatches>

## Adding new options

If the desired color is not available background colour utility, you can create
a new add a new class within `src/sass/globals/vars/colours.sass`. For the example,
we are using `$colourAlto`

<adding-new-colours></adding-new-colours>
