---
title: Breakpoints
description: All breakpoints available to use
---


The grid system comes with a 12 point grid system built using flex-box. The grid
is used to create specific layouts within an application’s content. It contains
5 types of media breakpoints that are used for targeting specific screen sizes
or orientations, xs, sm, md, lg and xl. These resolutions are defined below in
the Viewport Breakpoints table.

| Device                 | Code | Description                     | Range                  |
| ---------------------- | ---- | ------------------------------- | ---------------------- |
| Small mobile           | xs   | Devices under 320px             | &lt; 320px             |
| Mobile                 | xs   | Devices between 320px and 768px | 320px &gt; &lt; 767px  |
| Small table            | md   | Tablets between 768px and 980px | 768px &gt; &lt; 979px  |
| Small desktop/ tablets | lg   | Tablets between 768px and 980px | 980px &gt; &lt; 1200px |
| Desktop                | xl   | Desktop devices over 1200px     | &gt; 1200px            |

## Using these breakpoints

There are serval breakpoint variables for handing responsive SASS. When writing
SASS, we recommend mobile first, thus using the UP is the primary query typ we use.

### Above a breakpoint

These will only apply above a specific viewport.

| SASS variable name  | Value                                          |
| ------------------- | ---------------------------------------------- |
| `$breakpoint-xs-up` | (min-width: #{$xsSize})                        |
| `$breakpoint-sm-up` | (min-width: #{map-get($grid-breakpoints, sm)}) |
| `$breakpoint-md-up` | (min-width: #{map-get($grid-breakpoints, md)}) |
| `$breakpoint-lg-up` | (min-width: #{map-get($grid-breakpoints, lg)}) |
| `$breakpoint-xl-up` | (min-width: #{map-get($grid-breakpoints, xl)}) |

### Below a breakpoint

These will only apply below a specific viewport.

| SASS variable name    | Value                                                 |
| --------------------- | ----------------------------------------------------- |
| `$breakpoint-xs-down` | (max-width: #{$xsSize - 0.02})                        |
| `$breakpoint-sm-down` | (max-width: #{map-get($grid-breakpoints, sm) - 0.02}) |
| `$breakpoint-md-down` | (max-width: #{map-get($grid-breakpoints, md) - 0.02}) |
| `$breakpoint-lg-down` | (max-width: #{map-get($grid-breakpoints, lg) - 0.02}) |
| `$breakpoint-xl-down` | (max-width: #{map-get($grid-breakpoints, xl) - 0.02}) |

### Specific breakpoint only

We try to avoid using only breakpoint as it can causing confusing behavior.

| SASS variable name     | Value                                                                                                    |
| ---------------------- | -------------------------------------------------------------------------------------------------------- |
| `$breakpoint-xxs-only` | (max-width: #{$xsSize - 0.02})                                                                           |
| `$breakpoint-xs-only`  | (min-width: #{$xsSize}) and (max-width: #{map-get($grid-breakpoints, sm) - 0.02})                        |
| `$breakpoint-sm-only`  | (min-width: #{map-get($grid-breakpoints, sm)}) and (max-width: #{map-get($grid-breakpoints, md) - 0.02}) |
| `$breakpoint-md-only`  | (min-width: #{map-get($grid-breakpoints, md)}) and (max-width: #{map-get($grid-breakpoints, lg) - 0.02}) |
| `$breakpoint-lg-only`  | (min-width: #{map-get($grid-breakpoints, lg)}) and (max-width: #{map-get($grid-breakpoints, xl) - 0.02}) |
| `$breakpoint-xl-only`  | (min-width: #{map-get($grid-breakpoints, xl)})                                                           |
