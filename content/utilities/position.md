---
title: Position

---

| Type        | Class               |
| ----------- | ------------------- |
| Absolute    | `position-absolute` |
| Relative    | `position-relative` |
| Sticky [^1] | `position-sticky`   |
| Fixed       | `position-fixed`    |
| Static      | `position-static`   |

[^1]: On IE 11, a polyfill is used, however this does not operate the same as the native `position: sticky`
