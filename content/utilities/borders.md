---
title: Borders
description: Border and border colours
borderColours:
  - border-primary
  - border-secondary
  - border-success
  - border-danger
  - border-warning
  - border-info
  - border-light
  - border-grey-light
  - border-dark
  - border-white
borderRadius:
  - rounded
  - rounded-top
  - rounded-right
  - rounded-bottom
  - rounded-left
  - rounded-circle
  - rounded-pill
  - rounded-0
borders:
  - top
  - right
  - bottom
  - left

sizes:
  - lg
  
roundedSizes:
  - rounded-sm
  - rounded-lg
---


<border-example :borders="borders" :radius="borderRadius" :colours="borderColours" :sizes="sizes" :rounded-sizes="roundedSizes"></border-example>
