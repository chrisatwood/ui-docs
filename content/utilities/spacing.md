---
title: Spacing
description: Spacing utilities
---

## How it works

The helper classes apply margin or padding to an element ranging from 0 to 40px.
These classes can be applied using the following format {property}{direction}-{size}.

## Property

The **property** applies the type of spacing:

- `m` - applies margin
- `p` - applies padding

## Direction

The **direction** designates the side the property applies to:

- **t**: Applies the spacing for `margin-top` and `padding-top`
- **b**: Applies the spacing for <code>margin-bottom</code> and `padding-bottom`
- **l**: Applies the spacing for <code>margin-left</code> and `padding-left`
- **r**: Applies the spacing for <code>margin-right</code> and `padding-right`
- **x**: Applies the spacing for both <code>*-left</code> and `*-right`
- **y**: Applies the spacing for both <code>*-top</code> and  `*-bottom`
- **a**: Applies the spacing for the property in all directions <Badge colour="warning">Upcoming v3.x</Badge>

## Size

The **size** controls the increment of the property in 5px and 10px intervals:

- **0**: Sets `margin` or `padding` to 0px <Badge colour="info">Bootstrap</Badge>
- **1**: Sets `margin` or `padding` to 0.25rem (4px) <Badge colour="info">Bootstrap</Badge>
- **2**: Sets `margin` or `padding` to 0.5rem (8px) <Badge colour="info">Bootstrap</Badge>
- **3**: Sets `margin` or `padding` to to 1rem (16px) <Badge colour="info">Bootstrap</Badge>
- **4**: Sets `margin` or `padding` to 1.5rem (28px) <Badge colour="info">Bootstrap</Badge>
- **5**: Sets `margin` or `padding` to 5px
- **10**: Sets `margin` or `padding` to 10px
- **15**: Sets `margin` or `padding` to 15px
- **20**: Sets `margin` or `padding` to 20px
- **30**: Sets `margin` or `padding` to 30px
- **40**: Sets `margin` or `padding` to 40px
- **50**: Sets `margin` or `padding` to 50px
- **12**: Sets `margin` or `padding` to 48px
- **13**: Sets `margin` or `padding` to 52px
- **14**: Sets `margin` or `padding` to 56px
- **15**: Sets `margin` or `padding` to 60px
- **n1**: Sets `margin` or `padding` to -0.25rem (-4px) <Badge colour="info">Bootstrap</Badge>
- **n2**: Sets `margin` or `padding` to 0.5rem (-8px) <Badge colour="info">Bootstrap</Badge>
- **n3**: Sets `margin` or `padding` to to -1rem (-16px) <Badge colour="info">Bootstrap</Badge>
- **n4**: Sets `margin` or `padding` to -1.5rem (-28px) <Badge colour="info">Bootstrap</Badge>
- **-5**: Sets `margin` or `padding` `top` or `bottom` -5px <Badge colour="warning">Changing to n5 in v3.x</Badge>
- **auto**: Sets `margin`or `padding` to auto

```js{1,3-5}[server.js]
const http = require('http')
const bodyParser = require('body-parser')

http.createServer((req, res) => {
  bodyParser.parse(req, (error, body) => {
    res.end(body)
  })
}).listen(3000)
```
