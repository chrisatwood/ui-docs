---
title: Buttons
external: https://getbootstrap.com/docs/4.3/components/buttons/

files:
  - /src/sass/components/buttons/button.sass
sizes:
  - { text: 'Small', value: 'sm' }
  - { text: 'Normal', value: '' }
  - { text: 'Large', value: 'lg' }

colours:
  - Primary
  - Success
  - Warning
  - Info
  - Inverse
  - Danger
  - Link


---

<button-example title="Buttons" :sizes="sizes" :colours="colours"></button-example>

## Button tags

The .btn classes are designed to be used with the `<a>` element. However, you can also use these classes on `<button>`
or `<input>` elements (though some browsers may apply a slightly different rendering).

When using button classes on `<a>` elements that are used to trigger in-page functionality (like collapsing content),
rather than linking to new pages or sections within the current page, these links should be given a role="button" to
appropriately convey their purpose to assistive technologies such as screen readers.

## Outline buttons

In need of a button, but not the hefty background colors they bring? Replace the default modifier classes with the
`.btn-outline-*` ones to remove all background images and colors on any button.

## Checked or radio buttons

Bootstrap’s .button styles can be applied to other elements, such as `<label>`s, to provide checkbox or radio style
button toggling. Add data-toggle="buttons" to a .btn-group containing those modified buttons to enable their toggling
behavior via JavaScript and add .btn-group-toggle to style the `<input>`s within your buttons [^3].

The checked state for these buttons is only updated via click event on the button. If you use another method to update
the input —e.g., with `<input type="reset">` or by manually applying the input’s checked property — you’ll need to toggle
.active on the `<label>` manually. [^4]

## States

There are various state the button could have including active, disabled, loading, focused and visited.

### Active

Active state can be achieved natively via the :active pseudo class[^1] or via the .active class applied to the button.
On majority of button variations, it will applied a darker background colour to denote it's is "selected/ active".

For accessibility, you **must** add aria attributes to help screen readers understand this. Please use `aria-pressed="true"`.

### Loading

Each of the default button state can have a loading state. Once the `btn--loading` class has been added, simple spinner
will replace the text and icons of the button.
### Disabled

Disabled state has not been fully implemented across all buttons. However, disabled state should be used whenever the button
should not be pressed or that functionality of the button is not yet available i.e. if JS has not yet loading.

To disable the button, you should use `disabled` class and for `button` and `input`, you must also add `disabled` attribute.
### Focus

The focus state is the same as global focus state, [please read more here](/content/focus).

## Methods

| Method                  | Description                                                                     |
| ----------------------- | ------------------------------------------------------------------------------- |
| `$().button('toggle')`  | Toggles push state. Gives the button the appearance that it has been activated. |
| `$().button('dispose')` | Destroys an element’s button.                                                   |

[^1]: `:active` is only available on `button` elements, not for `input` or `a`
[^2]: This is not yet implemented on outline buttons.
[^3]: Note that you can create single input-powered buttons or groups of them.
[^4]: Note that pre-checked buttons require you to manually add the `active` class to the input’s `<label>`.
