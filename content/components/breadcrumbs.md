---
title: Breadcrumbs
files:
  - /src/sass/components/breadcrumb/breadcrumb.sass
  
external: https://getbootstrap.com/docs/4.3/components/breadcrumb/
---
## Functionality

On desktop devices, the breadcrumb should contain the current page and at most 3 previous section. If there any more,
you should use use `&hellip`[^1].

On mobile, only direct parent and the current page should be shown due to the limit space available. If ther are any more,
you should use use `&hellip`[^1].

Every breadcrumb item has `text-overflow: ellipsis;`, thus, when the breadcrumb no longer fits on one line,
all breadcrumb items.

## Seperator

Indifferent to Bootstrap implementation, the separator is manually added into the HTML. The separator `li` class
should be `breadcrumb-item-separator`.
## Accessibility

1. The `nav` must have an `aria-label` or `aria-labelledby` attribute
2. The current page's `li` must have `aria-current="page"`.
3. Every separator `li` must have `aria-hidden="true"` so screen readers will skip the item.

## Preview

<breadcrumb-example></breadcrumb-example>

[^1]: This is not done automatically, when creating your template, you must explicitly add `d-none` or`d-lg-block` classes for the given item to be shown correctly.
