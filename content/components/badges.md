---
title: Badges and labels
external: https://getbootstrap.com/docs/4.3/components/badge/
files: 
  - src/sass/components/label-badge/label-badge.sass
colours:
  - Primary
  - Success
  - Warning
  - Info
  - Inverse
  - Danger
  - Light
  - Dark


headings:
  - h1
  - h2
  - h3
  - h4
  - h5
  - {tag: "button", class: "btn btn-primary mb-20"}

  
sizes:
  - Large
  
colour: success
---

## Which class?

Both `label` and `badge` classes have the same styles, however it's recommended to use the `badge` class rather than `label` as
it will be deprecated in a future release.

## Placement

Badges can be places with headings and button, please note, badges height does not change based on where it's nested.
Badges can also be applied to normal anchor links.

<badge-heading-example :headings="headings"></badge-heading-example>

## Preview

<badge-example :colours="colours" :colour="colour" :sizes="sizes"></badge-example>

## Accessibility

When using a colour to convey a message, you must also add the appropriate `aria-label` or `aria-labelledby` to allow screen
readers to understand the meaning.
