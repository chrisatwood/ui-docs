---
title: Accordions
badge: Great
files:
  - /src/sass/components/accordions.sass
  - /src/ts/components/accordion.ts
  - /src/ts/components/accordion-CPD.ts

sizes:
  - Small
  - Standard
  - Large

alignments:
  - {text: 'Right', class: 'float-right ml-auto'}

contentTypes:
  - Basic
  - Rich

appearances:
  - {text: 'Link', class: 'accordion--link'}
---

<accordion-example title="Accordions" :sizes="sizes" :content-types="contentTypes" :appearances="appearances" :alignments="alignments"></accordion-example>

## When yo use accordions

<alert colour="info">Coming soon</alert>

## How it works

Under the hood, this uses the [collapse](/components/collapse) data attributes.
There are additional TypeScript that generates a expand/ close all accordions.
## Allowed content within accordions

Contents of an accordion can contain any HTML element, videos and maps. You can also create columns within
the accordion body.

## Types

### Basic accordions

By default, all accordions with more than 2 items will be generated with a expand/ close all button.

In order to disable this, you can `data-collapseAll="false"`.

### Sizing and appearances

There are 3 sizing options, small, standard and large. These will alter the font size, padding and icon size of the header.
