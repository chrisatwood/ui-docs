---
title: Installation
description: Learn how to install the repository
---

## Clone repo

Clone the repository on to your local machine.

<command-line ssh="$ git clone git@git.cardiff.ac.uk:dev-team/squiz/tools/asset-manager.git" https="$ git clone https://git.cardiff.ac.uk/dev-team/squiz/tools/asset-manager.git"></command-line>

## Install modules

Install the dependencies required to run the development server.

<command-line yarn="$ yarn install" npm="$ npm install"></command-line>

## Run development server

A development server will spin up on port `8080`
<command-line yarn="$ yarn dev" npm="$ npm run dev"></command-line>
