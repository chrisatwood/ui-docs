---
title: Browser support
description: List of currently supported browsers
---

Below is the the browser which we activily support. If the browser/ version is
not listed, if CSS Grid is supported on that version, then the UI kit should
operated as normal.

## List

| Browser         | Support level                            |
| :-------------- | :--------------------------------------- |
| IE              | 10 partial [^1], 11 functional [^2] [^3] |
| Edge            | 18, 87+                                  |
| Firefox         | 84+                                      |
| Chrome          | 87+                                      |
| Safari          | 12+                                      |
| Samsung browser | 11+                                      |

[^1]: IE 10 was dropped during 2.3.X.
[^2]: Current features in 2.X will all be functional in IE 11, however, it may not appear the exact same compared to Chrome.
[^3]: IE 11 will be dropped in 3.0.X.

## How we test our features

All desktop browsers are be tested on all breakpoints and various in between
breakpoints. Each component works regardless of screen width. For mobile devices,
testing is carried out on both portrait and landscape. All components, unless
explicitly documented, are updated when screen resizes or screen rotates.

We actively test against the latest version of each browser vendor, however, we
do no re-test on older versions of the browser. We do not anticipate any issues,
however, if you come across a browser inconsistencies contact [web@cardiff.ac.uk](web@cardiff.ac.uk)
