---
title: Accessibility requirements
description: Assisted labels, semantics, ARIA roles, colour contrast requirement and more
---
## Assisted labels

We recommend adding `aria-label` or `aria-labelledby` where ever possible as it provides more context for
users using screen readers. The use of `aria-description` and `aria-describedby` are also high recommended.
Please the see the definition of the properties above here.

Whilst using the `section` tag, you **must** provide either `aria-label` or `aria-labelledby`, this typically will point
to the section's highest level.

## Semantics

HTML5 tags should be used, please unsure the correct tags are used in the right context.

### Examples

| Tag          | Use                                                                                       |
| ------------ | ----------------------------------------------------------------------------------------- |
| `P`          | Should used for sentences                                                                 |
| `span`       | Should used for labels                                                                    |
| `table`      | Should used for displaying tabular data                                                   |
| `main`       | Should used for main content of a page                                                    |
| `nav`        | Should used for any navigation or sub-navigation                                          |
| `section`    | Should used splitting document into self-contained sections                               |
| `blockquote` | Should used for display quotes                                                            |
| `strong`     | Should used for bold. **Do not use `b`**.                                                 |
| `button`     | Should used for where the user is not redirect to another page i.e. Button that fires JS. |
| `a`          | Should used for where the user is redirect to another page.                               |
| `fieldset`   | Should used for wrapping related form inputs together. this is combined with `legend`.    |
| `figcaption` | Fig caption should always be added given there are caption is available.                  |
| `time`       | This should be used whether displaying time-related content.                              |

[Please see the full list of tags here](https://www.w3schools.com/tags/default.asp)
## Attaching the correct ARIA roles

All elements must also have the appropriate ARIA roles. [Please find these here](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques).
Note, some aria roles are covered by HTML5 tags.

## Color contrast

All Bootstrap and Cardiff colour palette must conform to the WCAG AA (Level 2). In order verify this,
we recommend the [Wave](https://chrome.google.com/webstore/detail/wave-evaluation-tool/jbbplnpkjmmeebjpijfedlgcdilocofh),
[VizBug](https://chrome.google.com/webstore/detail/visbug/cdockenadnadldjbbgcallicgledbeoc?hl=en) or
in-built Browser tools to ensure any new features are a minimum of WCAG AA.

Please note, during testing, it's a required step of any merge request to fully test you features using the Wave too.

## Visually hidden content

Content which should be visually hidden, but remain accessible to assistive technologies such as screen readers,
can be styled using the .sr-only class. This can be useful in situations where additional visual information or ues
(such as meaning denoted through the use of color) need to also be conveyed to non-visual users.

## Skipping content

Every page must have a skip to content link as the first-tabbable item.
This link should scroll the user to the main content area, usually within the main tag.

## Reduced motion

All animation and transition should support the `prefers-reduce-motion` media query. This
feature tells the browser if the user has opted in to see less animations. All CSS transitions and animations
will automatically be set `transition-duration: 0s;` and `animation-duration: 0s`. However, if you animate
with TypeScript, you must add condition in TypeScript that checks the `prefers-reduce-motion` before animating.
