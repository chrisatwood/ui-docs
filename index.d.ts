export interface MenuItem {
  name: string
  url: string
  items?: MenuItem[]
}

export interface Colour {
  value: string
  name: string
  exactMatch: boolean
  variableName?: string
  classNames?: string[]
}
export interface ColourItem {
  name: string
  value: string
  textColour?: string
}
export interface Ratio {
  ratio?: string
  name: string
  value: string
}

export interface TableOfContentItem {
  [index: number]: TableOfContentItem
  id: string
  text: string
  depth: number
  items: TableOfContentItem[]
}

export interface SiteMapItem {
  title: string
  path: string
  items: SiteMapItem[]
}
export interface SiteMap {
  gettingStarted: SiteMapItem | null
  components: SiteMapItem | null
  content: SiteMapItem | null
  utilities: SiteMapItem | null
  typescriptAPI: SiteMapItem | null
}

export interface ContentItem {
  path: string
  dir: string
  title: string
}
